function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    if(letter.length == 1){
        let count = 0;
        for(let index=0; index < sentence.length; index++){
            if(sentence[index] === letter) count++;
        }
        return count;
    }
    else return undefined;
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    text = text.toLowerCase();
    let temp_text = text; 
    let isThereRepeatingLetter = false;
    for(let index1=0; index1 < text.length; index1++){
        for(let index2=0; index2 < temp_text.length; index2++){
            if(index1 != index2 && text[index1] === temp_text[index2]){
                isThereRepeatingLetter = true;
                break;
            }
        }
    }
    if(isThereRepeatingLetter) return false;
    return true;
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    if(age < 13) return undefined;
    else if((age >= 13 && age <= 21) || age > 64){
        let discounted_price = price * 0.8;
        return discounted_price.toFixed(2);
    }
    else if(age >= 22 && age <= 64) return price.toFixed(2);
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    
    let categories_with_no_stocks = items.map(item => {
        if(!item.stock) return item.category;
    });

    let unique_array = [];
    categories_with_no_stocks.forEach(item => {
        if(!unique_array.includes(item)){
            unique_array.push(item);
        }
    });
    return unique_array;
}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
    let candidates = [];
    for(let index1=0; index1 < candidateA.length; index1++){
        for(let index2=0; index2 < candidateB.length; index2++){
            if(candidateA[index1] === candidateB[index2]) candidates.push(candidateA[index1]);
        }
    }
    return candidates;
}


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};